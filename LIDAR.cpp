#include "LIDAR.h"
#include "mbed.h"

LIDAR::LIDAR(PinName tx, PinName rx, PinName motor_pwm) :
    _lidar(tx, rx), _motor(motor_pwm)
{
    _lidar.baud(115200);
    _motor.period_us(100);
    _motor.write(0.0);
    
    _data_ptr = (char *) &_data; // kenapa ?? awalnya _data_ptr = (char*) _data dan ukuran data short 2* ukuran char
    _speed_ptr = (char *) &_speed; // kenapa ?? idem
    _intensity_ptr = (char *) &_intensity; // idem
    
    _fsm_state = 0;
    _fsm_count = 0;
    _fsm_angle = 0;

    checksum_h = 0;
    checksum_l = 0;
    
    _speed = 0;
    for(int i = 0; i < 360; i++) 
    {
        _data[i] = 0;
        _intensity[i] = 0;
        _invalid_flag[i] = false;
        _strength_flag[i] = false;
    }
}

void LIDAR::StartData(void)
{
    _lidar.attach(callback(this, &LIDAR::data_parser),Serial::RxIrq);
}

void LIDAR::StopData(void)
{
    _lidar.attach(NULL, Serial::RxIrq);
}

void LIDAR::SetPWMDuty(float duty)
{
    _motor.write(duty);
}

void LIDAR::SetPWMPeriodUs(int microseconds)
{
    _motor.period_us(microseconds);
}

float LIDAR::GetData(int degree)
{
    if(degree < 360 && degree >= 0) {
        return _data[degree]/10.0;
    }
    else {
        return -1.0;
    }
}

bool LIDAR::IsDataInvalid(int degree)
{
    if (degree < 360 && degree >= 0) {
        return _invalid_flag[degree];
    }
    else {
        return 0;
    }
}

bool LIDAR::GetStrengthFlag(int degree){
    if(degree < 360 && degree >= 0) {
        return _strength_flag[degree];
    }
    else {
        return 0;
    }
}

float LIDAR::GetSpeed(void)
{
    return _speed/64.0;
}

short LIDAR::GetIntensity(int degree) {
    if (degree < 360 && degree >= 0) {
        return _intensity[degree];
    } else {
        return 0;
    }
}

uint8_t LIDAR::GetChecksuml() {
    return checksum_l;
}

uint8_t LIDAR::GetChecksumh() {
    return checksum_h;
}

void LIDAR::data_parser(void)
{
    char buffer;
    
    // Insert data to temporary buffer
    buffer = _lidar.getc();

    // State machine for data extraction
    // <start> <index> <speed_L> <speed_H> [Data 0] [Data 1] [Data 2] [Data 3] <checksum_L> <checksum_H>

    switch(_fsm_state)
    {
        case 0:
        // If start byte found, move to next state
        if(buffer == LIDAR_START_BYTE)
        {
            _fsm_count = 0;
            _fsm_state++;
        }
        break;
        
        case 1:
        // Determine the packet number and check packet validity
        _fsm_angle = (buffer - LIDAR_FIRST_INDEX) << 3; // 0xA0 : indeks pertama
        if(_fsm_angle <= 712) // 712 = 89 (angka maksimum untuk indeks) *8 => 2 untuk tiap derajat dari 360
        {
            _fsm_state++;
        }
        else
        {
            _fsm_state = 0;
        }
        break;
        
        case 2:
        // Add the LSB of RPM
        _speed_ptr[0] = buffer; // char* _speed_ptr
        _fsm_state++;
        break;
        
        case 3:
        // Add the MSB of RPM
        _speed_ptr[1] = buffer; // nunjuk apa?
        _fsm_state++;
        break;
        
        case 4:
        // Add the LSB of distance
        _data_ptr[718 - _fsm_angle] = buffer; // kenapa 718 ?? char* _data_ptr
        // 718 = 359 * 2
        // 718 - _fsm_angle : berarti si pembuat driver original menganggap putaran lidar ke arah sudut negatif
        _fsm_state++;
        break;
        
        case 5:
        // Add the MSB of distance and check packet validity
        _invalid_flag[359 - (_fsm_angle >> 1)] = buffer & LIDAR_FINVALID_MASK; // 0x80 : bit 7 dari  byte 1 data bernilai 1
        _strength_flag[359 -(_fsm_angle >> 1)] = buffer & LIDAR_FSTRENGTH_MASK; // 0x40 : bit 6 dari byte 1 data bernilai 1
        _data_ptr[719 - _fsm_angle] = buffer & LIDAR_DATA_MSB_MASK; // 0x3F : dalam biner 3F = 0011 1111, berarti hanya membaca dari bit ke 5 s.d. 0 (MSB pembacaan jarak)
        // 719 = 359 * 2 + 1, karena _data_ptr[i] untuk i ganjil menyimpan lsb dari pembacaan jarak, sedangkan sekarang yang dibaca msb
        
        _fsm_state++;
        break;

        case 6:
        // read the lsb of signal strength
        _intensity_ptr[718 - _fsm_angle] = buffer;
                
        _fsm_state++;
        break;

        case 7 :
        // read the msb of signal strength, 
        // increment packet counter and _fsm_angle,
        // check number of data required
        _intensity_ptr[719 - _fsm_angle] = buffer;

        _fsm_count++;
        _fsm_angle += 2;
        
        if (_fsm_count < 4) {
            _fsm_state = 4;
        }
        else {
            _fsm_state = 8;
        }

        break;

        case 8 :
        // proses LSB dari checksum
        checksum_l = buffer;
        _fsm_state++;
        break;

        case 9 :
        // proses MSB dari checksum
        checksum_h = buffer;
        _fsm_state = 0;
        break;
        /*
        
        case 6:
        // Increment packet counter and angle
        _fsm_count++;
        _fsm_angle += 2; // kenapa 2 ?? karena byte untuk data jarak 2: untuk lsb dan msb
        _fsm_state++;
        break;
        
        case 7:
        // Check number of data accquired
        // 1 packet should contains 4 data
        if(_fsm_count < 4) 
        {
            _fsm_state = 4;
        }
        else
        {
            _fsm_state = 0;
        }
        break;

        */
        
        default:
        _fsm_state = 0;
        
        }
}
