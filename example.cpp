#include "mbed.h"
#include "LIDAR.h"

 // The tx pin is PC_10
 // The rx pin is PC_11
 // The PWM pin to control motor is PA_15
 LIDAR lidar (PC_10, PC_11, PA_15);

 // Activate serial to PC for debugging
 Serial pc(USBTX, USBRX);

int main() {
    // Set duty cycle for motor PWM
    lidar.SetPWMDuty(0.25);

    // Start data aquisition
    lidar.StartData();

    int i = 0;
    float data, speed;
    short intensity;
    bool invalid_flag, strength_flag;

    while (1) {
        // Aquire LIDAR data from angle 0, 45, 90, 135... 315
        // Then send it to serial PC with 9600 baud
        data = lidar.GetData(i);
        speed = lidar.GetSpeed();
        intensity = lidar.GetIntensity(i);
        invalid_flag = lidar.IsDataInvalid(i);
        strength_flag = lidar.GetStrengthFlag(i);
        
        pc.printf("Spd=%.1f; Sdt=%d; D=%.1f; I=%.1d; finvalid=%d; fstrength=%d\r", speed, i, data, intensity, invalid_flag, strength_flag);

        /*
        i += 90;
        if (i >= 360) {
            i = 0;
        }    
        */
        wait(0.2);
    }
}